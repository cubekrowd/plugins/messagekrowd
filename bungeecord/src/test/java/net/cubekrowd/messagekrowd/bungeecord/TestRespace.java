package net.cubekrowd.messagekrowd.bungeecord;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRespace {
    @Test
    public void testRespace() {
        assertEquals("test test", ChatListener.respace("test   test"));
        assertEquals("test test", ChatListener.respace("test   test"));
        assertEquals("test", ChatListener.respace("   test   "));
    }
}
