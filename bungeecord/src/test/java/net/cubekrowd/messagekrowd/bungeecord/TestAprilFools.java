package net.cubekrowd.messagekrowd.bungeecord;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.*;

public class TestAprilFools {

    @Test
    public void testPlainReverse() {
        assertEquals("", AprilFools.reverse(""));
        assertEquals("!olleH", AprilFools.reverse("Hello!"));

        // should not interpret these as colors
        assertEquals(",§§tset*§", AprilFools.reverse("§*test§§,"));
    }

    @Test
    public void testColorReverse() {
        assertEquals("§a§a", AprilFools.reverse("§a"));
        assertEquals("§aorkeL§a", AprilFools.reverse("§aLekro"));
        assertEquals("§aorkeL§a§a", AprilFools.reverse("§a§aLekro"));
        assertEquals("§ahalb§6 si§7 siht§a", AprilFools.reverse("§7this §6is §ablah"));
        assertEquals("§f§atset§6 si§7 siht§f", AprilFools.reverse("§7this §6is §atest§f"));
    }
}
