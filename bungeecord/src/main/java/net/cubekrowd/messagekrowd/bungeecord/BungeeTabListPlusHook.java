package net.cubekrowd.messagekrowd.bungeecord;

import codecrafter47.bungeetablistplus.api.bungee.BungeeTabListPlusAPI;

public class BungeeTabListPlusHook {
    public static void registerAFKVariable(MessageKrowdBungeeCordPlugin plugin) {
        BungeeTabListPlusAPI.registerVariable(plugin, new AFKVariable(plugin));
    }

    public static void registerTrackVariable(MessageKrowdBungeeCordPlugin plugin, String trackName) {
        BungeeTabListPlusAPI.registerVariable(plugin, new TrackWeightVariable(trackName));
    }
}
