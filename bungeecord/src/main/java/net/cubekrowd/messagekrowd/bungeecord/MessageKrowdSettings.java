package net.cubekrowd.messagekrowd.bungeecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.config.Configuration;

public class MessageKrowdSettings {
    public int afkTime;
    public String afkGoneFormat;
    public String afkBackFormat;

    public String silenceFormat;

    public List<ServerGroup> staticServerGroups;

    public List<String> spamProtectedServers;
    public int capsLimit;
    public String capsMessage;
    public int charLimit;
    public String charMessage;
    public int rateLimitPointsPerMessage;
    public int rateLimitThreshold;
    public String rateLimitMessage;

    public boolean replaceFullwidth;
    public boolean censorEnabled;
    public List<String> censoredServers;
    public List<String> censoredWords;
    public boolean censorBlock;
    public String censorMessage;

    public List<String> passthroughServers;
    public List<String> teamPrefixSuffixServers;
    public List<String> commandsToProcess;

    public String chatClearedFormat;

    public String messageFormat;
    public String afkMessageFormat;
    public String socialSpyFormat;

    public boolean load(MessageKrowdBungeeCordPlugin plugin, Configuration config) {
        if (config.getInt("version") != 4) {
            plugin.getLogger().severe(ChatColor.RED + "WRONG CONFIGURATION VERSION. "
                    + "PLEASE BACKUP AND DELETE THE CONFIG.YML. The latest "
                    + "version will be generated automatically during next "
                    + "restart if you have moved the old one.");
            return false;
        }

        afkTime = config.getInt("afk-time");
        afkGoneFormat = config.getString("afk-gone-format");
        afkBackFormat = config.getString("afk-back-format");

        var linkedServers = config.getSection("linked-servers");
        staticServerGroups = new ArrayList<>();
        for (var key : linkedServers.getKeys()) {
            var group = new ServerGroup(key.toLowerCase(Locale.ENGLISH));
            group.serverNames.addAll(linkedServers.getStringList(key));
            staticServerGroups.add(group);
        }

        silenceFormat = config.getString("chat-silence-format");
        spamProtectedServers = config.getStringList("spam-protection.servers");
        capsLimit = config.getInt("spam-protection.caps-limit");
        capsMessage = config.getString("spam-protection.caps-message");
        charLimit = config.getInt("spam-protection.char-limit");
        charMessage = config.getString("spam-protection.char-message");
        rateLimitPointsPerMessage = config.getInt("spam-protection.rate-limit.points-per-message");
        rateLimitThreshold = config.getInt("spam-protection.rate-limit.limit-threshold");
        rateLimitMessage = config.getString("spam-protection.rate-limit.limit-message");

        replaceFullwidth = config.getBoolean("replace-fullwidth", true);
        censorEnabled = config.getBoolean("censor.enabled");
        censoredServers = config.getStringList("censor.servers");
        censoredWords = config.getStringList("censor.list");
        censorBlock = config.getBoolean("censor.block");
        censorMessage = config.getString("censor.message");

        teamPrefixSuffixServers = config.getStringList("use-team-prefix-suffix");
        passthroughServers = config.getStringList("passthrough-servers");
        commandsToProcess = config.getStringList("process-commands");

        chatClearedFormat = config.getString("chat-cleared-format");

        messageFormat = config.getString("message-format");
        afkMessageFormat = config.getString("afk-pm-format");
        socialSpyFormat = config.getString("socialspy-format");
        return true;
    }

    public ServerGroup getServerGroupByServer(ServerInfo server) {
        for (var group : staticServerGroups) {
            if (group.serverNames.contains(server.getName())) {
                return group;
            }
        }

        // NOTE(traks): return group of just the server if no match. This allows
        // dynamically registering of servers if we ever need it
        var res = new ServerGroup(server.getName());
        res.serverNames.add(server.getName());
        return res;
    }

    public List<ServerGroup> gatherGroups() {
        var res = new ArrayList<>(staticServerGroups);

        // NOTE(traks): now add groups for servers with no static group
        // (i.e. missing from config)
        for (var si : ProxyServer.getInstance().getServers().values()) {
            ServerGroup foundGroup = null;
            for (var group : res) {
                if (group.serverNames.contains(si.getName())) {
                    foundGroup = group;
                    break;
                }
            }
            if (foundGroup != null) {
                continue;
            }

            var newGroup = new ServerGroup(si.getName());
            newGroup.serverNames.add(si.getName());
            res.add(newGroup);
        }
        return res;
    }
}
