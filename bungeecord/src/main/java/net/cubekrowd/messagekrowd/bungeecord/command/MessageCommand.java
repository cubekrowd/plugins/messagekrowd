package net.cubekrowd.messagekrowd.bungeecord.command;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdBungeeCordPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MessageCommand extends Command {

    private final MessageKrowdBungeeCordPlugin plugin;

    public MessageCommand(MessageKrowdBungeeCordPlugin plugin) {
        super("message", null, "m", "msg", "w", "whisper", "tell");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Please use: /m <player> <message>");
            return;
        }
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(ChatColor.RED + "This is a player-only command");
            return;
        }
        var pp = (ProxiedPlayer) sender;
        var receiver = plugin.matchOnlinePlayer(args[0]);

        if (receiver == null) {
            sender.sendMessage(ChatColor.RED + "Error: Player " + args[0] + " not found");
            return;
        }

        String[] messageArray = Arrays.copyOfRange(args, 1, args.length);
        String message = String.join(" ", messageArray);

        UUID ppuuid = pp.getUniqueId();
        var settings = plugin.globalSettings;

        if (sender.hasPermission("messagekrowd.formatcodes")) {
            message = ChatColor.translateAlternateColorCodes('&',
                    message).replaceAll(ChatColor.MAGIC + "", "");
        }
        if (sender.hasPermission("messagekrowd.formatcodeslimited")) {
            message = (message.replaceAll("&m", "" + ChatColor.STRIKETHROUGH));
            message = (message.replaceAll("&n", "" + ChatColor.UNDERLINE));
            message = (message.replaceAll("&o", "" + ChatColor.ITALIC));
            message = (message.replaceAll("&r", "" + ChatColor.RESET));
        }

        var spyMessage = ChatColor.translateAlternateColorCodes('&', settings.socialSpyFormat)
                .replace("{sender}", sender.getName()).replace("{target}", receiver.getName())
                .replace("{message}", message);

        synchronized (plugin.globalLock) {
            // wrap all this stuff inside the lock so PMs are always consistent
            // with the last entry in the reply map
            if (ppuuid.equals(receiver.getUniqueId())) {
                var msg = ChatColor.translateAlternateColorCodes('&', settings.messageFormat)
                        .replace("{sender}", "me").replace("{target}", "me")
                        .replace("{message}", message);
                sender.sendMessage(msg);
            } else {
                var msgForSender = ChatColor.translateAlternateColorCodes('&', settings.messageFormat)
                        .replace("{sender}", "me").replace("{target}", receiver.getName())
                        .replace("{message}", message);
                var msgForTarget = ChatColor.translateAlternateColorCodes('&', settings.messageFormat)
                        .replace("{sender}", sender.getName()).replace("{target}", "me")
                        .replace("{message}", message);
                sender.sendMessage(msgForSender);
                receiver.sendMessage(msgForTarget);
            }

            // Warn the sender if the receiver is AFK
            if (plugin.isAFK(receiver)) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.afkMessageFormat));
            }

            plugin.replyMap.put(ppuuid, receiver.getUniqueId());
            plugin.replyMap.put(receiver.getUniqueId(), ppuuid);

            // send spy messages
            plugin.getProxy().getPlayers().stream()
                    .filter(u -> !u.equals(sender))
                    .filter(u -> !u.equals(receiver))
                    .filter(u -> plugin.allSocialSpies.containsKey(u.getUniqueId()))
                    .forEach(u -> u.sendMessage(TextComponent.fromLegacyText(spyMessage)));
        }

        if (plugin.hasESAPI) {
            Map<String, String> data = new HashMap<>();
            data.put("s", ppuuid.toString());
            data.put("r", receiver.toString());
            data.put("m", message);
            EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "pm", data));
        }
    }
}
