package net.cubekrowd.messagekrowd.bungeecord;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.logging.Level;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class KrowdListener implements Listener {

    private final MessageKrowdBungeeCordPlugin plugin;

    public KrowdListener(MessageKrowdBungeeCordPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        // @NOTE(traks) client connected to BungeeCord and BungeeCord just
        // switched protocol state to PLAY and just sent login success packet.

        var pp = e.getPlayer();
        var uuid = pp.getUniqueId();
        var timeNow = System.nanoTime();

        plugin.onlineDataMap.compute(uuid, (k, v) -> {
            // NOTE(traks): refresh player data always, even if the player is
            // online multiple times (emulate new session start)
            var res = new OnlinePlayerData();
            if (v == null) {
                res.onlineCount = 1;
            } else {
                res.onlineCount = v.onlineCount + 1;
            }
            res.lastSeenMillis.set(timeNow);
            return res;
        });

        // Remove social-spy if they are no longer staff
        if (!pp.hasPermission("messagekrowd.socialspy")) {
            if (plugin.allSocialSpies.remove(pp.getUniqueId()) != null) {
                plugin.saveSocialSpies();
                e.getPlayer().sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                        + "Turned socialspy " + ChatColor.RED + "off");
                plugin.getLogger().info("Turned socialspy off for " + pp.getName() + " because they don't have permission");
            }
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        // @NOTE(traks) fired if player disconnects after post login happened

        var pp = e.getPlayer();
        var uuid = e.getPlayer().getUniqueId();
        var newOnlineData = plugin.onlineDataMap.compute(uuid, (k, v) -> {
            if (v != null) {
                v.onlineCount--;
                if (v.onlineCount <= 0) {
                    return null;
                }
            }
            return v;
        });

        if (newOnlineData != null) {
            plugin.getLogger().info(String.format(Locale.ENGLISH,
                    "Player %s disconnected while online %s times",
                    pp.getName(), newOnlineData.onlineCount + 1));
        }
    }

    @EventHandler
    public void onTabComplete(TabCompleteEvent e) {
        // Return if they are doing an unknown command
        List<String> aliases = Arrays.asList("message", "m", "msg", "w", "whisper", "tell", "reply", "r", "find");
        if (e.getCursor().startsWith("/") && !aliases.contains(e.getCursor().toLowerCase().split(" ")[0].substring(1))) {
            return;
        }

        int p = e.getCursor().lastIndexOf(" ");
        String lastArg = e.getCursor().substring(p + 1);
        String lowerArg = lastArg.toLowerCase(Locale.ENGLISH);

        plugin.getProxy().getPlayers().stream()
                .map(ProxiedPlayer::getName)
                .filter(u -> u.toLowerCase(Locale.ENGLISH).startsWith(lowerArg))
                .filter(u -> !e.getSuggestions().contains(u))
                .forEach(e.getSuggestions()::add);
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        try {
            if (e.getTag().equals(plugin.getMessageChannelName())) {
                e.setCancelled(true);

                if (!(e.getSender() instanceof Server)) {
                    return;
                }
                ServerInfo server = ((Server) e.getSender()).getInfo();
                ByteArrayInputStream b = new ByteArrayInputStream(e.getData());
                DataInputStream in = new DataInputStream(b);
                String subChannel = in.readUTF();

                var settings = plugin.globalSettings;

                switch (subChannel) {
                case "DisplayTeam":
                    if (settings.teamPrefixSuffixServers.contains(server.getName())) {
                        var uuid = UUID.fromString(in.readUTF());
                        var newDisplayName = in.readUTF();
                        var pp = plugin.getProxy().getPlayer(uuid);
                        if (pp != null) {
                            var onlineData = plugin.getOnlineData(pp);

                            if (onlineData == null) {
                                plugin.getLogger().info(String.format(Locale.ENGLISH, "Received %s for offline player %s", subChannel, pp.getName()));
                                return;
                            }

                            onlineData.displayNameByServer.put(server.getName(), newDisplayName);
                        }
                    }
                    break;
                case "Move": {
                    var uuid = UUID.fromString(in.readUTF());
                    var pp = plugin.getProxy().getPlayer(uuid);
                    if (pp != null) {
                        var onlineData = plugin.getOnlineData(pp);

                        if (onlineData == null) {
                            plugin.getLogger().info(String.format(Locale.ENGLISH, "Received %s for offline player %s", subChannel, pp.getName()));
                            return;
                        }

                        onlineData.markSeen(plugin, pp);
                    }
                    break;
                }
                }
            }
        } catch (Exception ex) {
            plugin.getLogger().log(Level.SEVERE, "Failed to handle plugin message", ex);
        }
    }
}
